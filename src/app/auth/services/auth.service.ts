import { Token } from './../models/token.model';
import { LoginResponse } from './../models/login-response.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { User } from '../models/user.model';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  readonly BASE_URL = environment.url;
  readonly LOGIN_URL = `${this.BASE_URL}login`;
  loggedUser: User;
  private jwtHelperService = new JwtHelperService();

  constructor(private http: HttpClient) {}

  login$(user: User): Observable<LoginResponse> {
    return this.http.post<LoginResponse>(this.LOGIN_URL, user).pipe(
      tap((LoginResponse) => {
        this.storeToken(LoginResponse.accessToken);
      })
    );
  }

  logOut(): void {
    localStorage.removeItem('access_token');
    location.replace(`${location.origin}/auth`);
  }

  storeToken(token: string): void {
    localStorage.setItem('access_token', token);
  }

  decodeToken(): Token | null {
    const token = localStorage.getItem('access_token');
    if (!token) {
      return null;
    }
    return this.jwtHelperService.decodeToken(token);
  }

  getLoggedUser(): Observable<User> {
    const token = this.decodeToken();
    return this.http.get<User>(this.BASE_URL + `users/${token?.sub}`).pipe(
      tap((response) => {
        this.loggedUser = response;
      })
    );
  }

  isAccessTokenExpired(): boolean {
    const token = localStorage.getItem('access_token');
    if (!token) {
      return true;
    }

    /*Check for invalid token*/

    try {
      return this.jwtHelperService.isTokenExpired(token);
    } catch {
      return true;
    }
  }
}
