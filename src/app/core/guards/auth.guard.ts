import { Observable } from 'rxjs';
import { AuthService } from './../../auth/services/auth.service';
import { CanLoad, Router, UrlSegment } from '@angular/router';
import { Route } from '@angular/compiler/src/core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanLoad {
  constructor(private router: Router, private authService: AuthService) {}

  canLoad(
    route: Route,
    segments: UrlSegment[]
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (this.authService.isAccessTokenExpired()) {
      this.router.navigate(['auth']);
      return false;
    } else {
      return true;
    }
  }
}
