import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthenticationsMenuComponent } from './authentications-menu.component';

describe('AuthenticationsMenuComponent', () => {
  let component: AuthenticationsMenuComponent;
  let fixture: ComponentFixture<AuthenticationsMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AuthenticationsMenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthenticationsMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
