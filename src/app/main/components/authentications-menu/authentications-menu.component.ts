import { AuthenticationsListDialogComponent } from './../authentications/authentications-list-dialog/authentications-list-dialog.component';
import { take } from 'rxjs/operators';
import { AuthenticationsSaveDialogComponent } from './../authentications/authentications-save-dialog/authentications-save-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { Authentication } from './../../models/authentication.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-authentications-menu',
  templateUrl: './authentications-menu.component.html',
  styleUrls: ['./authentications-menu.component.scss'],
})
export class AuthenticationsMenuComponent implements OnInit {
  constructor(
    private saveDialog: MatDialog,
    private listAuthentications: MatDialog
  ) {}

  ngOnInit(): void {}

  openAuthentications(event?: MouseEvent): void {
    if (event) {
      event.stopPropagation();
    }
    let dialogRef = this.listAuthentications.open(
      AuthenticationsListDialogComponent,
      {
        width: '800px',
      }
    );

    dialogRef.afterClosed().pipe(take(1)).subscribe();
  }

  editAuthentication(authentication: Authentication, event?: MouseEvent): void {
    if (event) {
      event.stopPropagation();
    }
    let dialogRef = this.saveDialog.open(AuthenticationsSaveDialogComponent, {
      width: '500px',
      data: { authentication: authentication },
    });

    dialogRef.afterClosed().pipe(take(1)).subscribe();
  }

  addAuthentication(): void {
    this.editAuthentication(new Authentication());
  }
}
