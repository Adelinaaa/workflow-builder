import { AuthSchemaProperty } from './../../../models/authschema-property.model';
import { AbstractControl, FormGroup } from '@angular/forms';
import { Directive, Input } from '@angular/core';

@Directive()
export abstract class AuthBase {
  @Input() property: AuthSchemaProperty;
  @Input() required: boolean;
  @Input() propertyName: string;
  @Input() formAuthGroup: AbstractControl;
}
