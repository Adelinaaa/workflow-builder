import { AuthSelectComponent } from './../inputs/auth-select/auth-select.component';
import { AuthInputNumberComponent } from './../inputs/auth-input-number/auth-input-number.component';
import { AuthInputTextComponent } from './../inputs/auth-input-text/auth-input-text.component';
import { Component, Type } from '@angular/core';
import { AuthBase } from '../auth-base/auth-base.component';

@Component({
  selector: 'app-auth-wrapper',
  templateUrl: './auth-wrapper.component.html',
  styleUrls: ['./auth-wrapper.component.scss'],
})
export class AuthWrapperComponent extends AuthBase {
  get component(): Type<AuthBase> {
    if (this.property.enum) {
      return AuthSelectComponent;
    }
    switch (this.property.type) {
      case 'string':
        return AuthInputTextComponent;
      case 'integer':
        return AuthInputNumberComponent;
      default:
        return AuthInputTextComponent;
    }
  }
}
