import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthenticationsDeleteDialogComponent } from './authentications-delete-dialog.component';

describe('AuthenticationsDeleteDialogComponent', () => {
  let component: AuthenticationsDeleteDialogComponent;
  let fixture: ComponentFixture<AuthenticationsDeleteDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AuthenticationsDeleteDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthenticationsDeleteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
