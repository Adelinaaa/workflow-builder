import { take } from 'rxjs/operators';
import { AuthenticationsService } from './../../../services/authentications.service';
import { Authentication } from './../../../models/authentication.model';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-authentications-delete-dialog',
  templateUrl: './authentications-delete-dialog.component.html',
  styleUrls: ['./authentications-delete-dialog.component.scss'],
})
export class AuthenticationsDeleteDialogComponent implements OnInit {
  authentication: Authentication;

  constructor(
    private authenticationsService: AuthenticationsService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialog: MatDialogRef<AuthenticationsDeleteDialogComponent>
  ) {
    this.authentication = data.authentication;
  }

  ngOnInit(): void {}

  onConfirmDelete(authentication: Authentication) {
    this.authenticationsService
      .delete$(authentication.id)
      .pipe(take(1))
      .subscribe({
        next: () => {
          this.authenticationsService.subject$.next();
          this.dialog.close(authentication);
        },
      });

  }

  onCancel(): void {
    this.dialog.close();
  }
}
