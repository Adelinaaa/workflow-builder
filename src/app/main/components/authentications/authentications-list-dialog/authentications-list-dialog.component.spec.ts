import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthenticationsListDialogComponent } from './authentications-list-dialog.component';

describe('AuthenticationsListDialogComponent', () => {
  let component: AuthenticationsListDialogComponent;
  let fixture: ComponentFixture<AuthenticationsListDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AuthenticationsListDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthenticationsListDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
