import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-authentications-list-dialog',
  templateUrl: './authentications-list-dialog.component.html',
  styleUrls: ['./authentications-list-dialog.component.scss'],
})
export class AuthenticationsListDialogComponent implements OnInit {
  constructor(
    private dialog: MatDialogRef<AuthenticationsListDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {}

  close(): void {
    this.dialog.close();
  }
}
