import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthenticationsSaveDialogComponent } from './authentications-save-dialog.component';

describe('AuthenticationsSaveDialogComponent', () => {
  let component: AuthenticationsSaveDialogComponent;
  let fixture: ComponentFixture<AuthenticationsSaveDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AuthenticationsSaveDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthenticationsSaveDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
