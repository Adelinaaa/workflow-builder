import { User } from 'src/app/auth/models/user.model';
import { AuthService } from 'src/app/auth/services/auth.service';
import { AuthSchema } from './../../../models/authschema.model';
import { AuthschemasService } from './../../../services/authschemas.service';
import { take } from 'rxjs/operators';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, OnInit, Inject } from '@angular/core';
import { Authentication } from 'src/app/main/models/authentication.model';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { AuthenticationsService } from 'src/app/main/services/authentications.service';

@Component({
  selector: 'app-authentications-save-dialog',
  templateUrl: './authentications-save-dialog.component.html',
  styleUrls: ['./authentications-save-dialog.component.scss'],
})
export class AuthenticationsSaveDialogComponent implements OnInit {
  authentication: Authentication;
  authSchemas: AuthSchema[];
  selectedAuthSchema: AuthSchema;
  formGroup: FormGroup;
  loggedUser: User;

  constructor(
    private authchemasService: AuthschemasService,
    private authenticationsService: AuthenticationsService,
    private fb: FormBuilder,
    private authService: AuthService,
    private dialog: MatDialogRef<AuthenticationsSaveDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.authentication = data.authentication;
    this.loggedUser = this.authService.loggedUser;
  }

  ngOnInit(): void {
    this.buildForm();
    this.getAuthSchemas();
  }

  close(): void {
    this.dialog.close();
  }

  private buildForm(): void {
    this.formGroup = this.fb.group({
      id: [this.authentication.id],
      name: [this.authentication.name, Validators.required],
      description: [this.authentication.description],
      serviceName: [null, Validators.required],
      userId: [this.loggedUser.id],
      created: [new Date()],
    });
  }

  onSubmit(): void {
    if (this.formGroup.invalid) {
      this.formGroup.get('data')?.markAllAsTouched();
      return;
    }

    this.authenticationsService
      .save$(this.formGroup.value)
      .pipe(take(1))
      .subscribe({
        next: (result) => {
          this.authenticationsService.subject$.next();
          this.dialog.close(result);
        },
      });
  }

  private getAuthSchemas(): void {
    this.authchemasService
      .getAll$()
      .pipe(take(1))
      .subscribe({
        next: (authschemas: AuthSchema[]) => {
          this.authSchemas = authschemas;
        },
      });
  }

  onChange(event: any) {
    const targetSchema = this.authSchemas.find(
      (a) => a.serviceName === event.value
    );
    if (targetSchema) {
      this.selectedAuthSchema = targetSchema;
      this.buildAuthSchemaForm();
    }
  }

  private buildAuthSchemaForm(): void {
    //Clear form group for previously selected authschema
    this.formGroup.removeControl('properties');

    let authSchemaFormGroup = this.fb.group({});

    Object.keys(this.selectedAuthSchema.schema.properties).forEach((key) => {
      let property = this.selectedAuthSchema.schema.properties[key];
      let control = new FormControl(property.default || null);

      if (this.selectedAuthSchema.schema.required.includes(key)) {
        control.setValidators(Validators.required);
      }
      authSchemaFormGroup.addControl(key, control);
    });

    this.formGroup.addControl('data', authSchemaFormGroup);
  }
}
