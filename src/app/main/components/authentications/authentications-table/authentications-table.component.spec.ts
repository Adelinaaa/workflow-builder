import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthenticationsTableComponent } from './authentications-table.component';

describe('AuthenticationsTableComponent', () => {
  let component: AuthenticationsTableComponent;
  let fixture: ComponentFixture<AuthenticationsTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AuthenticationsTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthenticationsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
