import { MatSort } from '@angular/material/sort';
import { User } from 'src/app/auth/models/user.model';
import { AuthenticationsSaveDialogComponent } from './../authentications-save-dialog/authentications-save-dialog.component';
import { take, takeUntil } from 'rxjs/operators';
import { AuthenticationsDeleteDialogComponent } from './../authentications-delete-dialog/authentications-delete-dialog.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { AuthenticationsService } from 'src/app/main/services/authentications.service';
import { AuthService } from 'src/app/auth/services/auth.service';
import { Authentication } from 'src/app/main/models/authentication.model';
import { MatTableDataSource } from '@angular/material/table';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-authentications-table',
  templateUrl: './authentications-table.component.html',
  styleUrls: ['./authentications-table.component.scss'],
})
export class AuthenticationsTableComponent implements OnInit {
  loggedUser: User;
  authentications: Authentication[];
  displayedColumns: string[] = [
    'service',
    'name',
    'created',
    'description',
    'actions'
  ];
  dataSource: MatTableDataSource<Authentication>;
  destroy$ = new Subject<boolean>();

  constructor(
    private authService: AuthService,
    private authenticationsService: AuthenticationsService,
    private deleteDialog: MatDialog,
    private saveDialog: MatDialog,
    private matSnackBar: MatSnackBar
  ) {
    this.loggedUser = this.authService.loggedUser;
    this.authenticationsService.subject$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.getAuthentications();
      });
  }

  ngOnInit(): void {
    this.getAuthentications();
  }

  applyFilter(filterValue: string) {
    if (this.dataSource) {
      this.dataSource.filterPredicate = (
        data: Authentication,
        filter: string
      ): boolean => {
        return data.name.toLowerCase().includes(filter);
      };
    }
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  private getAuthentications(): void {
    this.authenticationsService
      .getAll$()
      .pipe(take(1))
      .subscribe({
        next: (authentications: Authentication[]) => {
          this.authentications = authentications;
          this.dataSource = new MatTableDataSource(
            authentications.filter((a) => a.userId == this.loggedUser.id)
          );
        },
      });
  }

  deleteAuthentication(
    authentication: Authentication,
    event?: MouseEvent
  ): void {
    if (event) {
      event.stopPropagation();
    }
    let dialogRef = this.deleteDialog.open(
      AuthenticationsDeleteDialogComponent,
      {
        data: { authentication: authentication },
        width: '300px',
      }
    );
    dialogRef
      .afterClosed()
      .pipe(take(1))
      .subscribe({
        next: (result) => {
          if (result) {
            this.matSnackBar.open('Authentication is deleted!', 'Cancel', {
              duration: 2000,
            });
          }
          this.getAuthentications();
        },
      });
  }

  editAuthentication(authentication: Authentication, event?: MouseEvent): void {
    if (event) {
      event.stopPropagation();
    }
    let dialogRef = this.saveDialog.open(AuthenticationsSaveDialogComponent, {
      data: { authentication: authentication },
      width: '500px',
    });

    dialogRef
      .afterClosed()
      .pipe(take(1))
      .subscribe({
        next: (result) => {
          if (result) {
            this.matSnackBar.open('Authentication is saved!', 'OK', {
              duration: 2000,
            });
          }
          this.getAuthentications();
        },
      });
  }

  addAuthentication(): void {
    this.editAuthentication(new Authentication());
  }
}
