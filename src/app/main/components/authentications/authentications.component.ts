import { MatSnackBar } from '@angular/material/snack-bar';
import { User } from 'src/app/auth/models/user.model';
import { AuthService } from 'src/app/auth/services/auth.service';
import { MatTableDataSource } from '@angular/material/table';
import { AuthenticationsSaveDialogComponent } from './authentications-save-dialog/authentications-save-dialog.component';
import { Authentication } from './../../models/authentication.model';
import { take } from 'rxjs/operators';
import { AuthenticationsService } from './../../services/authentications.service';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-authentications',
  templateUrl: './authentications.component.html',
  styleUrls: ['./authentications.component.scss'],
})
export class AuthenticationsComponent implements OnInit {
  authentications: Authentication[];

  constructor(
    private matSnackBar: MatSnackBar,
    private authenticationsService: AuthenticationsService,
    private saveDialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.getAuthentications();
  }

  private getAuthentications(): void {
    this.authenticationsService
      .getAll$()
      .pipe(take(1))
      .subscribe({
        next: (authentications: Authentication[]) => {
          this.authentications = authentications;
        },
      });
  }

  editAuthentication(authentication: Authentication, event?: MouseEvent): void {
    if (event) {
      event.stopPropagation();
    }
    let dialogRef = this.saveDialog.open(AuthenticationsSaveDialogComponent, {
      data: { authentication: authentication },
      width: '500px',
    });

    dialogRef
      .afterClosed()
      .pipe(take(1))
      .subscribe({
        next: (result) => {
          if (result) {
            this.matSnackBar.open('Authentication is saved!', 'OK', {
              duration: 2000,
            });
            this.getAuthentications();
          }
        },
      });
  }

  addAuthentication(): void {
    this.editAuthentication(new Authentication());
  }
}
