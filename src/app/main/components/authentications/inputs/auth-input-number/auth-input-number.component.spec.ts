import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthInputNumberComponent } from './auth-input-number.component';

describe('AuthInputNumberComponent', () => {
  let component: AuthInputNumberComponent;
  let fixture: ComponentFixture<AuthInputNumberComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AuthInputNumberComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthInputNumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
