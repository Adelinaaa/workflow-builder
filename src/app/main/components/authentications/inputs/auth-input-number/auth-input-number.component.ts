import { FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AuthBase } from '../../auth-base/auth-base.component';

@Component({
  selector: 'app-auth-input-number',
  templateUrl: './auth-input-number.component.html',
  styleUrls: ['./auth-input-number.component.scss']
})
export class AuthInputNumberComponent extends AuthBase {
  get formGroup(): FormGroup {
    return this.formAuthGroup as FormGroup
  }
}
