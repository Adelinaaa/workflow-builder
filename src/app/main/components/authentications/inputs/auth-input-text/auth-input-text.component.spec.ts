import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthInputTextComponent } from './auth-input-text.component';

describe('AuthInputTextComponent', () => {
  let component: AuthInputTextComponent;
  let fixture: ComponentFixture<AuthInputTextComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AuthInputTextComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthInputTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
