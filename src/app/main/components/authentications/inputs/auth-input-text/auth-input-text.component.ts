import { FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AuthBase } from '../../auth-base/auth-base.component';

@Component({
  selector: 'app-auth-input-text',
  templateUrl: './auth-input-text.component.html',
  styleUrls: ['./auth-input-text.component.scss']
})
export class AuthInputTextComponent extends AuthBase{
  get formGroup(): FormGroup {
    return this.formAuthGroup as FormGroup
  }
}
