import { FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AuthBase } from '../../auth-base/auth-base.component';

@Component({
  selector: 'app-auth-select',
  templateUrl: './auth-select.component.html',
  styleUrls: ['./auth-select.component.scss'],
})
export class AuthSelectComponent extends AuthBase {
  get formGroup(): FormGroup {
    return this.formAuthGroup as FormGroup;
  }
}
