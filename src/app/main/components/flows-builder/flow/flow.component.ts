import { AuthService } from 'src/app/auth/services/auth.service';
import { Authentication } from 'src/app/main/models/authentication.model';
import { AuthenticationsService } from 'src/app/main/services/authentications.service';

import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from '@angular/forms';
import { Flow } from './../../../models/flow.model';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { take, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-flow',
  templateUrl: './flow.component.html',
  styleUrls: ['./flow.component.scss'],
})
export class FlowComponent implements OnInit, OnDestroy {
  @Input() flow: Flow;
  formGroup: FormGroup;
  authentications: Authentication[];

  destroy$ = new Subject<boolean>();

  constructor(
    private formBuilder: FormBuilder,
    private authenticationsService: AuthenticationsService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.buildForm();
    this.checkFlowForAuthentications();
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  private checkFlowForAuthentications() {
    let authKeyVariable = this.flow.process.variables.find(
      (v) => !!v.meta.authType
    );

    if (authKeyVariable) {
      const loggedUserId = this.authService.loggedUser.id;
      const authType = authKeyVariable.meta.authType;

      this.getFlowAuthentications(authType, loggedUserId);

      this.authenticationsService.subject$
        .pipe(takeUntil(this.destroy$))
        .subscribe(() => this.getFlowAuthentications(authType, loggedUserId));
    }
  }

  private getFlowAuthentications(authType: string, userId: number): void {
    this.authenticationsService
      .getAll$()
      .pipe(take(1))
      .subscribe({
        next: (authentications: Authentication[]) => {
          // show all options if auth type is "basic" (by requirement)
          this.authentications =
            authType === 'Common/BasicAuth'
              ? authentications
              : authentications.filter(
                  (a) => a.userId === userId && a.serviceName === authType
                );
        },
      });
  }

  private buildForm(): void {
    this.formGroup = this.formBuilder.group({});

    this.flow.process.variables.forEach((variable) => {
      if (variable.required) {
        this.formGroup.addControl(
          variable.name,
          new FormControl(null, Validators.required)
        );
      } else {
        this.formGroup.addControl(variable.name, new FormControl(null));
      }
    });
  }
}
