import { FormGroup } from '@angular/forms';
import { FlowVariable } from './../../../../models/flow-variable.model';
import { Directive, Input } from "@angular/core";

@Directive()
export abstract class InputBase {
  @Input() variable: FlowVariable;
  @Input() formGroup: FormGroup;

  @Input() options?: any[]; // Dropdown options if the input is type select
  @Input() displayNameAccessor?: string | null;
  @Input() displayValueAccessor?: string | null;

}
