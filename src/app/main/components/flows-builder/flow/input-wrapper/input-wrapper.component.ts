import { SelectComponent } from './../inputs/select/select.component';
import { InputCheckboxComponent } from './../inputs/input-checkbox/input-checkbox.component';
import { InputNumberComponent } from './../inputs/input-number/input-number.component';
import { InputTextComponent } from './../inputs/input-text/input-text.component';
import { ChangeDetectionStrategy, Component, Type } from '@angular/core';
import { InputBase } from '../input-base/input-base.component';

@Component({
  selector: 'app-input-wrapper',
  templateUrl: './input-wrapper.component.html',
  styleUrls: ['./input-wrapper.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InputWrapperComponent extends InputBase {
  get component(): Type<InputBase> {
    if(this.variable.schema?.enum || this.variable.meta.authType){
      return SelectComponent
    }
    switch (this.variable.schema.type) {
      case 'number':
        return InputNumberComponent;
      case 'integer':
        return InputNumberComponent;
      case 'boolean':
        return InputCheckboxComponent;
      default:
        return InputTextComponent;
    }
  }
}
