import { Component } from '@angular/core';
import { InputBase } from '../../input-base/input-base.component';

@Component({
  selector: 'app-input-checkbox',
  templateUrl: './input-checkbox.component.html',
  styleUrls: ['./input-checkbox.component.scss'],
})
export class InputCheckboxComponent extends InputBase {}
