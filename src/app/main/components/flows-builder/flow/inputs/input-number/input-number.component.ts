import { Component } from '@angular/core';
import { InputBase } from '../../input-base/input-base.component';

@Component({
  selector: 'app-input-number',
  templateUrl: './input-number.component.html',
  styleUrls: ['./input-number.component.scss'],
})
export class InputNumberComponent extends InputBase {}
