import { Component } from '@angular/core';
import { InputBase } from '../../input-base/input-base.component';

@Component({
  selector: 'app-input-text',
  templateUrl: './input-text.component.html',
  styleUrls: ['./input-text.component.scss'],
})
export class InputTextComponent extends InputBase {}
