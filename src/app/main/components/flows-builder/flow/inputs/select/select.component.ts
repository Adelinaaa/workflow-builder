import { Component } from '@angular/core';
import { InputBase } from '../../input-base/input-base.component';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
})
export class SelectComponent extends InputBase {}
