import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlowsBuilderComponent } from './flows-builder.component';

describe('FlowsBuilderComponent', () => {
  let component: FlowsBuilderComponent;
  let fixture: ComponentFixture<FlowsBuilderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlowsBuilderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FlowsBuilderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
