import { take } from 'rxjs/operators';
import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { DirObj } from './../integrations/integrations.component';
import { Component, OnInit } from '@angular/core';
import { FlowsService } from '../../services/flows.service';
import { Flow } from '../../models/flow.model';

@Component({
  selector: 'app-flows-builder',
  templateUrl: './flows-builder.component.html',
  styleUrls: ['./flows-builder.component.scss'],
})
export class FlowsBuilderComponent implements OnInit {
  droppedIntegrations: DirObj[] = [];
  flows: Flow[] = [];

  constructor(private flowsService: FlowsService) {}

  ngOnInit(): void {}

  drop(event: CdkDragDrop<any>): void {
    if (event.container === event.previousContainer) {
      let draggedFlow = this.flows[event.previousIndex];
      this.flows.splice(event.previousIndex, 1);

      this.flows.splice(event.currentIndex, 0, draggedFlow);
      return;
    }

    let droppingFlowId = Number(
      event.previousContainer.data[event.previousIndex]['id']
    );

    this.flowsService
      .get$(droppingFlowId)
      .pipe(take(1))
      .subscribe({
        next: (flow: Flow) => this.flows.splice(event.currentIndex, 0, flow),
      });
  }
}
