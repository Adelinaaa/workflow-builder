import { take } from 'rxjs/operators';
import { Integration } from './../../models/integration.model';
import { IntegrationsService } from './../../services/integrations.service';
import { Component, OnInit } from '@angular/core';

export interface DirObj {
  id?: number;
  name: string;
  path: string;
  children: DirObj[];
}

@Component({
  selector: 'app-integrations',
  templateUrl: './integrations.component.html',
  styleUrls: ['./integrations.component.scss'],
})
export class IntegrationsComponent implements OnInit {
  integrations: Integration[];
  tree: DirObj[];
  currentSelectedFolder: DirObj;

  constructor(private integrationsService: IntegrationsService) {}

  get currentFolderPath(): string[] {
    return this.currentSelectedFolder.path.split('/').filter((p) => p !== '');
  }

  ngOnInit(): void {
    this.getIntegrations();
  }

  private getIntegrations(): void {
    this.integrationsService
      .getAll$()
      .pipe(take(1))
      .subscribe({
        next: (integrations: Integration[]) => {
          this.integrations = integrations;
          this.tree = this.createTree();

          this.currentSelectedFolder = this.tree[0]; // root folder
        },
      });
  }

  private createTree(): DirObj[] {
    this.getSplittedPaths();
    let treeDir: DirObj[] = [];

    for (let i in this.integrations) {
      const currentPath = this.integrations[i].splittedPath;
      let currentDir = treeDir;

      for (let j = 0; j < currentPath.length; j++) {
        const pathPartition = currentPath[j];

        const existingPath = this.findWhere(currentDir, pathPartition);

        if (existingPath) {
          currentDir = (existingPath as DirObj).children;
        } else {
          const partitionFullPath = currentPath.slice(0, j + 1).join('/'); // results in empty string if it's root level
          const newPartition: DirObj = {
            name: pathPartition,
            path: partitionFullPath || '/',
            children: [],
          };

          //Apply ids only to flows not folders
          if (pathPartition == this.integrations[i].name) {
            newPartition.id = this.integrations[i].id;
          }
          currentDir.push(newPartition);
          currentDir = newPartition.children;
        }
      }
    }

    return treeDir;
  }

  private findWhere(
    currentDir: DirObj[],
    pathPartition: string
  ): DirObj | boolean {
    let dirIdx = 0;

    while (
      dirIdx < currentDir.length &&
      currentDir[dirIdx]['name'] !== pathPartition
    ) {
      dirIdx++;
    }

    if (dirIdx < currentDir.length) {
      return currentDir[dirIdx];
    } else {
      return false;
    }
  }

  private getSplittedPaths(): string[][] {
    return this.integrations.map((i) => (i.splittedPath = i.path.split('/')));
  }

  selectFolder(folder: DirObj): void {
    if (folder.children.length > 0) {
      this.currentSelectedFolder = folder;
    }
  }

  goToPath(index: number) {
    let targetPath = '/' + this.currentFolderPath.slice(0, index + 1).join('/');
    let searchedFolder = this.searchTree(this.tree[0], targetPath); //starting search from the root folder
    if (searchedFolder) {
      this.currentSelectedFolder = searchedFolder;
    }
  }

  searchTree(element: DirObj, matchingPath: string): DirObj | null {
    if (element.path === matchingPath) {
      return element;
    } else {
      // If element's children don't have any children, it means they are flows
      // No need to search through them
      let elementChildFolders = element.children.filter(
        (el) => el.children.length > 0
      );

      if (elementChildFolders.length > 0) {
        let result = null;
        for (let i = 0; result == null && i < elementChildFolders.length; i++) {
          result = this.searchTree(elementChildFolders[i], matchingPath);
        }
        return result;
      }

      return null;
    }
  }
}
