import { FlowsBuilderComponent } from './components/flows-builder/flows-builder.component';
import { AuthenticationsComponent } from './components/authentications/authentications.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './main.component';
import { LoggedUserResolver } from '../core/resolvers/logged-user.resolver';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    resolve: { loggedUser: LoggedUserResolver },
    children: [
      {
        path: '',
        redirectTo: 'flows',
        pathMatch: 'full',
      },
      {
        path: 'flows',
        component: FlowsBuilderComponent
      },
      {
        path: 'authentications',
        component: AuthenticationsComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MainRoutingModule {}
