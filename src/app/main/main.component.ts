import { TranslateService } from '@ngx-translate/core';
import { User } from './../auth/models/user.model';
import { AuthService } from 'src/app/auth/services/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnInit {
  loggedUser: User;
  userName: string;

  constructor(
    private authService: AuthService,
    private translate: TranslateService
  ) {
    this.loggedUser = this.authService.loggedUser;
    this.getLoggedUserName();
  }

  ngOnInit(): void {}

  private getLoggedUserName(): void {
    let loggedUserName = this.loggedUser.email.substring(
      0,
      this.loggedUser.email.lastIndexOf('@')
    );
    this.userName =
      loggedUserName.charAt(0).toUpperCase() + loggedUserName.slice(1);
  }

  logOut(): void {
    this.authService.logOut();
  }

  useLanguage(language: string): void {
    this.translate.use(language);
  }
}
