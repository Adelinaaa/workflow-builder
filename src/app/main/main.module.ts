import { OutputVariablesPipe } from './pipes/output-variables.pipe';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialUIModule } from '../shared/material-ui/material-ui.module';
import { NgxdModule } from '@ngxd/core';

import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';
import { FlowComponent } from './components/flows-builder/flow/flow.component';
import { InputVariablesPipe } from './pipes/input-variables.pipe';
import { InputTextComponent } from './components/flows-builder/flow/inputs/input-text/input-text.component';
import { InputNumberComponent } from './components/flows-builder/flow/inputs/input-number/input-number.component';
import { SelectComponent } from './components/flows-builder/flow/inputs/select/select.component';
import { InputWrapperComponent } from './components/flows-builder/flow/input-wrapper/input-wrapper.component';
import { InputCheckboxComponent } from './components/flows-builder/flow/inputs/input-checkbox/input-checkbox.component';
import { FlowsBuilderComponent } from './components/flows-builder/flows-builder.component';
import { IntegrationsComponent } from './components/integrations/integrations.component';
import { AuthenticationsComponent } from './components/authentications/authentications.component';
import { AuthenticationsMenuComponent } from './components/authentications-menu/authentications-menu.component';
import { AuthenticationsDeleteDialogComponent } from './components/authentications/authentications-delete-dialog/authentications-delete-dialog.component';
import { AuthenticationsSaveDialogComponent } from './components/authentications/authentications-save-dialog/authentications-save-dialog.component';
import { AuthWrapperComponent } from './components/authentications/auth-wrapper/auth-wrapper.component';
import { AuthInputTextComponent } from './components/authentications/inputs/auth-input-text/auth-input-text.component';
import { AuthInputNumberComponent } from './components/authentications/inputs/auth-input-number/auth-input-number.component';
import { AuthSelectComponent } from './components/authentications/inputs/auth-select/auth-select.component';
import { AuthenticationsTableComponent } from './components/authentications/authentications-table/authentications-table.component';
import { AuthenticationsListDialogComponent } from './components/authentications/authentications-list-dialog/authentications-list-dialog.component';
import {TranslateModule} from '@ngx-translate/core';


@NgModule({
  declarations: [
    MainComponent,
    IntegrationsComponent,
    FlowsBuilderComponent,
    FlowComponent,
    InputVariablesPipe,
    OutputVariablesPipe,
    InputTextComponent,
    InputNumberComponent,
    SelectComponent,
    InputWrapperComponent,
    InputCheckboxComponent,
    AuthenticationsComponent,
    AuthenticationsMenuComponent,
    AuthenticationsDeleteDialogComponent,
    AuthenticationsSaveDialogComponent,
    AuthWrapperComponent,
    AuthInputTextComponent,
    AuthInputNumberComponent,
    AuthSelectComponent,
    AuthenticationsTableComponent,
    AuthenticationsListDialogComponent
  ],
  imports: [
    CommonModule,
    MaterialUIModule,
    MainRoutingModule,
    ReactiveFormsModule,
    NgxdModule,
    TranslateModule
  ],
})
export class MainModule {}
