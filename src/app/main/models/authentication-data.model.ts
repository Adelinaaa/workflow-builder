export class AuthenticationDataModel {
  token: string;
  username: string;
  destination: string;
  host: string;
  password: string;
  port: number;
  protocol: string;
}
