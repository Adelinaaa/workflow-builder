import { AuthenticationDataModel } from './authentication-data.model';
import { BaseModel } from 'src/app/auth/models/base.model';

export class Authentication extends BaseModel{
  userId: number;
  name: string;
  description: string;
  serviceName: string;
  data: AuthenticationDataModel;
}
