export interface AuthSchemaProperty {
  title: string;
  description: string;
  type: string;
  format: string;
  default: string;
  enum: string[];
}
