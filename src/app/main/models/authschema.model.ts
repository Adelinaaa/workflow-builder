import { AuthSchemaProperty } from './authschema-property.model';
export class AuthSchema {
  title: string;
  serviceName: string;
  schema: {
    title: string;
    type: string;
    required: string[];
    properties: {
      [key: string]: AuthSchemaProperty;
    };
  };
}
