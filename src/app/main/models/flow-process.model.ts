import { FlowVariable } from './flow-variable.model';

export class FlowProcess {
  variables: FlowVariable[];
}
