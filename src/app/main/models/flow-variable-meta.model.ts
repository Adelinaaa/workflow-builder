export class FlowVariableMeta {
  displayName: string;
  description: string;
  authType: string;
}
