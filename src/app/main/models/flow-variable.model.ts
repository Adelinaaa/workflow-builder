import { FlowVariableMeta } from './flow-variable-meta.model';

export class FlowVariable {
  name: string;
  required: boolean;
  isInput: boolean;
  isOutput: boolean;
  schema: {
    type: string;
    enum: [];
  };
  meta: FlowVariableMeta;
}
