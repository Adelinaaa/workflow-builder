import { FlowMeta } from './flow-meta.model';
import { FlowProcess } from './flow-process.model';

export class Flow {
  id: number;
  path: string;
  name: string;

  process: FlowProcess;
  meta: FlowMeta;
}
