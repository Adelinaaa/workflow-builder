import { BaseModel } from 'src/app/auth/models/base.model';

export class Integration extends BaseModel {
  path: string;
  splittedPath: string[];
  description: string;
  name: string;
  displayName: string;
}
