import { FlowVariable } from './../models/flow-variable.model';
import { Pipe, PipeTransform } from "@angular/core";

@Pipe({name: 'inputVariables'})
export class InputVariablesPipe implements PipeTransform {
  transform(variables: FlowVariable[]): any {
    return variables.filter(variable => variable.isInput)
  }
}
