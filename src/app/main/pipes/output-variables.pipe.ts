import { FlowVariable } from './../models/flow-variable.model';
import { PipeTransform } from '@angular/core';
import { Pipe } from '@angular/core';

@Pipe({name: 'outputVariables'})
export class OutputVariablesPipe implements PipeTransform {
  transform(variables: FlowVariable[]): FlowVariable[] {
    return variables.filter(variable => variable.isOutput)
  }
}
