import { Authentication } from './../models/authentication.model';
import { Observable, Subject } from 'rxjs';
import { environment } from './../../../environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationsService {
  readonly BASE_URL = environment.url;
  readonly AUTHENTICATIONS_URL = `${this.BASE_URL}660/authentications`;
  subject$ = new Subject();

  constructor(private http: HttpClient) {}

  getAll$(): Observable<Authentication[]> {
    return this.http.get<Authentication[]>(this.AUTHENTICATIONS_URL);
  }

  delete$(id: number): Observable<void> {
    return this.http.delete<void>(this.AUTHENTICATIONS_URL + '/' + id);
  }

  save$(authentication: Authentication): Observable<Authentication> {
    if (authentication.id) {
      return this.http.put<Authentication>(
        this.AUTHENTICATIONS_URL + '/' + authentication.id,
        authentication
      );
    } else {
      return this.http.post<Authentication>(
        this.AUTHENTICATIONS_URL,
        authentication
      );
    }
  }
}
