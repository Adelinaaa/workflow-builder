import { AuthSchema } from './../models/authschema.model';
import { Observable } from 'rxjs';
import { environment } from './../../../environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthschemasService {
  readonly BASE_URL = environment.url;
  readonly AUTHSCHEMAS_URL = `${this.BASE_URL}660/authschemas`;

  constructor(private http: HttpClient) {}

  getAll$(): Observable<AuthSchema[]> {
    return this.http.get<AuthSchema[]>(this.AUTHSCHEMAS_URL);
  }

  delete$(id: number): Observable<void> {
    return this.http.delete<void>(this.AUTHSCHEMAS_URL + '/' + id);
  }

}
