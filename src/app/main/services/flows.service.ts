import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { Flow } from '../models/flow.model';

@Injectable({
  providedIn: 'root',
})
export class FlowsService {
  readonly BASE_URL = environment.url;
  readonly FLOWS_URL = `${this.BASE_URL}660/flows`;

  constructor(private http: HttpClient) {}

  get$(id: number): Observable<Flow> {
    return this.http.get<Flow>(this.FLOWS_URL + '/' + id);
  }
}
