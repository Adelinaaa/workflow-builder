import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { Integration } from '../models/integration.model';

@Injectable({
  providedIn: 'root',
})
export class IntegrationsService {
  readonly BASE_URL = environment.url;
  readonly INTEGRATIONS_URL = `${this.BASE_URL}660/integrations`;

  constructor(private http: HttpClient) {}

  getAll$(): Observable<Integration[]> {
    return this.http.get<Integration[]>(this.INTEGRATIONS_URL);
  }
}
